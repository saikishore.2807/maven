[![build status](https://gitlab.devtst.go2uti.com/playground/mavenhello/badges/master/build.svg)](https://gitlab.devtst.go2uti.com/playground/mavenhello/commits/master)

This is a simple maven project to experiment with various options in gitlab.

See [maven-in-five-minutes.html](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)

Raw steps:

    mvn archetype:generate -DgroupId=com.go2uti.devops -DartifactId=mavenhello -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false

Move files from mavenhello to this dir

    mvn package
    
    java -cp target/mavenhello-1.0-SNAPSHOT.jar com.go2uti.devops.App


