package com.go2uti.devops;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
      printMain();
      printNamaste();
    }

    static void printMain() {
        System.out.println( "Hello World!" );
    }

    static void printNamaste() {
    	System.out.println("Namaste!");
	}
}
